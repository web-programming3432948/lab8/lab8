import { AppDataSource } from "./data-source";
import { Type } from "./entity/Type";

AppDataSource.initialize()
  .then(async () => {
    const typesRespository = AppDataSource.getRepository(Type)
    await typesRespository.clear()

    var type = new Type()
    type.id = 1
    type.name = "drink"
    await typesRespository.save(type)

    var type = new Type()
    type.id = 2
    type.name = "bakery"
    await typesRespository.save(type)

    var type = new Type()
    type.id = 3
    type.name = "food"
    await typesRespository.save(type)

    const types = await typesRespository.find({ order: {id: "asc"} })
    console.log(types)
  })
  .catch((error) => console.log(error));
