import { AppDataSource } from "./data-source";
import { Product } from "./entity/Product";
import { Type } from "./entity/Type";


AppDataSource.initialize()
  .then(async () => {

    const typesRepository = AppDataSource.getRepository(Type)
    const drinkType = await typesRepository.findOneBy({ id: 1 })
    const bakeryType = await typesRepository.findOneBy({ id: 2 })
    const foodType = await typesRepository.findOneBy({ id: 3 })

    const productsRespository = AppDataSource.getRepository(Product)
    await productsRespository.clear()

    var product = new Product()
    product.id = 1
    product.name = "Americano"
    product.price = 40
    product.type = drinkType
    await productsRespository.save(product)

    product = new Product()
    product.id = 2
    product.name = "Espresso"
    product.price = 45
    product.type = drinkType
    await productsRespository.save(product)

    product = new Product()
    product.id = 3
    product.name = "Coco"
    product.price = 50
    product.type = drinkType
    await productsRespository.save(product)

    product = new Product()
    product.id = 4
    product.name = "Cake 1"
    product.price = 75
    product.type = bakeryType
    await productsRespository.save(product)

    product = new Product()
    product.id = 5
    product.name = "Cake 2"
    product.price = 75
    product.type = bakeryType
    await productsRespository.save(product)

    product = new Product()
    product.id = 6
    product.name = "Somtum"
    product.price = 45
    product.type = foodType
    await productsRespository.save(product)

    const products = await productsRespository.find({ relations: { type: true } })
    console.log(products)

  })
  .catch((error) => console.log(error));
