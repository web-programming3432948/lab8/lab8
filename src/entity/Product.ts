import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, OneToMany } from "typeorm"
import { Type } from "./Type"
import { OrderItem } from "./OrderItem"

@Entity()
export class Product {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    price: number

    @CreateDateColumn()
    created: Date

    @UpdateDateColumn()
    updated: Date

    //1 product have 1 type
    @ManyToOne(() => Type, (type) => type.products)
    type: Type    

    @OneToMany(() => OrderItem, (orderItem) => orderItem.product)
    orderIems: OrderItem[]
}
