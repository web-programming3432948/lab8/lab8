import { AppDataSource } from "./data-source";
import { Role } from "./entity/Role";
import { User } from "./entity/User";

AppDataSource.initialize()
  .then(async () => {
    const usersRepository = AppDataSource.getRepository(User);
    const rolesRepository = AppDataSource.getRepository(Role)

    const adminRole = await rolesRepository.findOneBy({ id: 1})
    const userRole = await rolesRepository.findOneBy({ id: 2})

    await usersRepository.clear();
    console.log("Inserting a new user into the Memory...");
    // admin
    var user = new User();
    user.id = 1;
    user.email = "admin@email.com";
    user.password = "Pass@1234";
    user.gender = "male";
    user.roles = []
    user.roles.push(adminRole)
    user.roles.push(userRole)
    console.log("Inserting a new user into the Database...");
    await usersRepository.save(user);

    // const admin = await usersRepository.findOneBy({ id: 1 });
    // console.log(admin);

    // user1
    user = new User();
    user.id = 2;
    user.email = "user1@email.com";
    user.password = "Pass@1234";
    user.gender = "male";
    user.roles = []
    user.roles.push(userRole)
    console.log("Inserting a new user into the Database...");
    await usersRepository.save(user);

    // const user1 = await usersRepository.findOne({
    //   where: [{ id: 2}, {gender: "female" }],
    // });
    // const user1 = await usersRepository.findOneBy({ id: 2 });
    // console.log(user1);

    // user2
    user = new User();
    user.id = 3;
    user.email = "user2@email.com";
    user.password = "Pass@1234";
    user.gender = "female";
    user.roles = []
    user.roles.push(userRole)
    console.log("Inserting a new user into the Database...");
    await usersRepository.save(user);

    // const user2 = await usersRepository.findOneBy({ id: 3 });
    // console.log(user2);
    const users = await usersRepository.find({ relations: { roles: true }})
    console.log(JSON.stringify(users, null, 2))

    const roles = await rolesRepository.find({ relations: { users: true }})
    console.log(JSON.stringify(roles, null, 2))
  })
  .catch((error) => console.log(error));
